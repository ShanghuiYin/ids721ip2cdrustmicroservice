IDS721 Spring 2024 Individual Project 2 - Continuous Delivery of Rust Microservice
===============

## Demo Video

[Youtube Video Here](https://youtu.be/L99UBgtlR2g)
&nbsp;&nbsp;![YouTube Video Views](https://img.shields.io/youtube/views/L99UBgtlR2g)

> Screenshot

![img_1.png](img_1.png)

## Prerequisites

1. create the project
`cargo new oscars && cd oscars`

2. Implement main.rs and lib.rs

3. Setup cargo.toml file with additional dependencies
```toml
[dependencies]
actix-web = "4"
rand = "0.8"
```

4. run the project with `cargo run` and 
   1. go to `http://localhost:8080/` to see the welcome message.
      ![img.png](img.png)
   2. go to `http://localhost:8080/oscar` to see the random 96th oscar winner and nominee list.
       
      ![img_1.png](img_1.png)

## Rust Microservice Functionality

I create a simple actix Microservice for retrieve oscars 2024 award winner and nominee list. 

This actix Microservice has multiple routes:

1. type: "/" that returns a welcome message

2. type: "/oscar" that returns a random 96th oscar winner and nominee list.

## Docker Configuration

Set up Dockerfile for APP webdocker
```Dockerfile
FROM rust:latest as builder
ENV APP oscars
WORKDIR /usr/src/$APP
COPY . .
RUN cargo install --path .

FROM debian:buster-slim
RUN apt-get update && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/$APP /usr/local/bin/$APP
#export this actix web service to port 8080 and 0.0.0.0
EXPOSE 8080
CMD ["oscars"]
```

## CI/CD Pipeline

- go to the AWS cloud9 environment

![img_2.png](img_2.png)

- install Rust in that environment
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
![img_3.png](img_3.png)

- clone this repository to the AWS cloud9 via https
```bash
https://gitlab.com/ShanghuiYin/ids721ip2cdrustmicroservice.git
```

- go to the AWS ECR, create one, and click the `view push commands`

![img_4.png](img_4.png)

1. aws ecr
2. docker build
3. docker tag
4. docker push

![img_5.png](img_5.png)

- go to the AWS app runner, create new service, and deploy the docker image to the app runner

![img_6.png](img_6.png)

> The deployment trigger is set to be automatic, so whenever the code is pushed to the gitlab repository, the pipeline will be triggered and the new docker image will be deployed to the app runner.


## Reference

1. https://nogibjj.github.io/rust-tutorial/chapter_4.html
2. https://www.oscars.org/oscars/ceremonies/2024
3. https://www.youtube.com/watch?v=I3cEQ_7aD1A&t=1s&ab_channel=PragmaticAILabs