/*A library that returns back random fruit */

use rand::Rng;

//create an const array of 10 fruits
pub const OSCAR: [&str; 4] = [
    "ACTOR IN A LEADING ROLE

    WINNER

CILLIAN MURPHY
Oppenheimer

NOMINEES

BRADLEY COOPER
Maestro
COLMAN DOMINGO
Rustin
PAUL GIAMATTI
The Holdovers
JEFFREY WRIGHT
American Fiction",
    "ACTOR IN A SUPPORTING ROLE

    WINNER

ROBERT DOWNEY JR.
Oppenheimer

NOMINEES

STERLING K. BROWN
American Fiction
ROBERT DE NIRO
Killers of the Flower Moon
RYAN GOSLING
Barbie
MARK RUFFALO
Poor Things",
    "ACTRESS IN A LEADING ROLE

    WINNER

EMMA STONE
Poor Things;

NOMINEES

ANNETTE BENING
Nyad
LILY GLADSTONE
Killers of the Flower Moon
SANDRA HÜLLER
Anatomy of a Fall
CAREY MULLIGAN
Maestro",
    "ACTRESS IN A SUPPORTING ROLE

    WINNER

DA'VINE JOY RANDOLPH
The Holdovers;

NOMINEES

EMILY BLUNT
Oppenheimer
DANIELLE BROOKS
The Color Purple
AMERICA FERRERA
Barbie
JODIE FOSTER
Nyad
"];

//create a function that returns a random fruit
pub fn random_oscar() -> &'static str {
    let mut rng = rand::thread_rng();
    let random_index = rng.gen_range(0..OSCAR.len());
    OSCAR[random_index]
}