/*An actix Microservice that has multiple routes:
A.  / that turns a hello world
B. /oscars that returns a random fruit
*/

use actix_web::{get, App, HttpResponse, HttpServer, Responder};
//import the random fruit function from the lib.rs file
use oscars::random_oscar;

//create a function that returns a hello world
#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello! Welcome to the 96th Oscars")
}

//create a function that returns a random fruit
#[get("/oscar")]
async fn oscar() -> impl Responder {
    //print the random fruit
    println!("Random Award: {}", random_oscar());
    HttpResponse::Ok().body(random_oscar())
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    //add a print message to the console that the service is running
    println!("Running the service");
    HttpServer::new(|| {
        App::new()
            .service(hello)
            .service(oscar)
    })
        .bind("0.0.0.0:8080")?
        .run()
        .await
}
